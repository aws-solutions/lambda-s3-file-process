package com.lambda.s3.file.process;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicSessionCredentials;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.event.S3EventNotification.S3EventNotificationRecord;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.securitytoken.AWSSecurityTokenService;
import com.amazonaws.services.securitytoken.AWSSecurityTokenServiceClientBuilder;
import com.amazonaws.services.securitytoken.model.AssumeRoleRequest;
import com.amazonaws.services.securitytoken.model.AssumeRoleResult;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.Arrays;
import java.util.List;

public class Transmitter implements RequestHandler<S3Event, String> {


  private static final String TRANSMIT_PREFIX = "api";
  private static final String ARCHIVE_PREFIX = "archive/api";
  private static final String ERROR_PREFIX = "error/api";
  private static final String DESTINATION_S3_TARGETS = "DESTINATION_S3_TARGETS";
  private static final String CSV_TARGET_ENV = "CSV_TARGETS";
  private static final String ASSUME_ROLE_NAME = "ASSUME_ROLE_NAME";
  private static final String IDENTITY_BUCKET_MARKER = "IDENTITY_BUCKET_MARKER";
  private static final String BUCKET_PATH_SEPARATOR = ":";
  private static final String BUCKET_TYPE_SEPARATOR = "#";
  private static final String BUCKET_TYPE_LOCAL = "local";
  private static final String BUCKET_TYPE_REMOTE = "remote";
  private static final String TARGET_SEPARATOR = ",";
  private static final String MDM_PATH_MARKER = "/api";
  private static final String DEFAULT_SESSION_NAME = "sm191-rcm-transmit-lambda";

  private Gson gson = new GsonBuilder().create();

  private LambdaLogger logger = null;

  public LambdaLogger getLogger() {
    return logger;
  }

  public void setLogger(LambdaLogger logger) {
    this.logger = logger;
  }


  @Override
  public String handleRequest(S3Event s3Event, Context context) {
    LambdaLogger logger = context.getLogger();
    setLogger(logger);
    AmazonS3 originalS3Client = AmazonS3ClientBuilder.defaultClient();

    logger.log("S3Event: " + gson.toJson(s3Event));

    S3EventNotificationRecord s3EventNotificationRecord = s3Event.getRecords().get(0);

    //get source bucket details
    String srcBucket = s3EventNotificationRecord.getS3().getBucket().getName();
    String srcKey = s3EventNotificationRecord.getS3().getObject().getUrlDecodedKey();

    String assumeRoleName = System.getenv(ASSUME_ROLE_NAME);
    String identityBucketMarker = System.getenv(IDENTITY_BUCKET_MARKER);

    List<String> targets = getTargets(DESTINATION_S3_TARGETS);

    //to check
    if (srcKey.contains(IDENTITY_BUCKET_MARKER)) {
      targets = getTargets(CSV_TARGET_ENV);
    }

    if (targets == null ||
        assumeRoleName == null ||
        identityBucketMarker == null) {
      logger.log(
          "ENVIRONMENT_ERROR: environmental variables like assumeRoleName, identityBucketMarker, targets cann't be null.");
      System.exit(-1);
    }

    logger.log("target for " + srcKey + " are " + gson.toJson(targets));
    logger.log("EDP role " + assumeRoleName + " and marker " + identityBucketMarker);

    targets
        .stream()
        .forEach(target -> {

          List<String> bucketTypes = Arrays.asList(target.split(BUCKET_TYPE_SEPARATOR));
          String bucketType = bucketTypes.get(0).trim();

          List<String> targetParts = Arrays.asList(bucketTypes.get(1).split(BUCKET_PATH_SEPARATOR));
          String targetS3 = targetParts.get(0).trim();
          String basePath = targetParts.get(1).trim();

          logger.log("targetS3:" + targetS3 + " and  basePath:" + basePath);
          //replacing source path with target path: transmit/[basePath]
          String targetKey = srcKey.replace(TRANSMIT_PREFIX, basePath);

          AmazonS3 S3ClientToUseForTransfer = originalS3Client;

          try {

            logger.log(
                "TRANSFER_ATTEMPT: Attempting to move file"
                    + srcKey
                    + " to "
                    + targetS3
                    + "/"
                    + targetKey);

            logger.log(
                "Detected marker " + identityBucketMarker + " Assuming role " + assumeRoleName);
            S3ClientToUseForTransfer = getS3ClientRole(assumeRoleName,
                originalS3Client.getRegionName());

            //always get source object from local s3 client
            S3Object s3Object = originalS3Client.getObject(new GetObjectRequest(srcBucket, srcKey));

            if (bucketType.equalsIgnoreCase(BUCKET_TYPE_LOCAL)) {
              logger.log("bucket type identified as :" + BUCKET_TYPE_LOCAL);
              originalS3Client.putObject(
                  targetS3,
                  targetKey,
                  s3Object.getObjectContent(),
                  s3Object.getObjectMetadata()
              );
            } else {
              logger.log("bucket type identified as :" + BUCKET_TYPE_REMOTE);
              S3ClientToUseForTransfer.putObject(
                  targetS3,
                  targetKey,
                  s3Object.getObjectContent(),
                  s3Object.getObjectMetadata()
              );
            }

          } catch (Exception e) {
            logger.log(
                "TRANSFER_FAILED :" + targetS3 + "/" + targetKey + "[" + e.getMessage() + "]"
            );
            moveToError(originalS3Client, srcBucket, srcKey);
            System.exit(-1);
          }
          logger.log(
              "TRANSFER_ATTEMPT_SUCCESSFUL : transferred "
                  + srcBucket
                  + "/"
                  + " to "
                  + targetS3
                  + "/"
                  + targetKey);


        });

    //move file to archive area
    String archiveKey = srcKey.replace(TRANSMIT_PREFIX, ARCHIVE_PREFIX);
    logger.log(
        "ARCHIVE_ATTEMPT :  attempting to move file to archival location"
            + srcKey
            + " to "
            + archiveKey);
    if (!move(originalS3Client, srcBucket, srcKey, archiveKey)) {

      logger.log("ARCHIVE_FAILED : " + srcBucket + "/" + archiveKey);
      System.exit(-1);
    }
    logger.log("ARCHIVE_SUCCESSFUL : File has been moved to " + archiveKey);

    return "Ok";
  }


  public List<String> getTargets(String targetKey) {
    String targetValues = System.getenv(targetKey);
    return Arrays.asList(targetValues.split(TARGET_SEPARATOR));
  }

  private AmazonS3 getS3ClientRole(String roleArnToAssume, String clientRegion) {

    //creating the STS client is part of your trusted code. it has
    // the security credentials you use to obtain temporary security credentials
    AWSSecurityTokenService stsClient = AWSSecurityTokenServiceClientBuilder.standard()
        .withCredentials(DefaultAWSCredentialsProviderChain.getInstance())
        .withRegion(clientRegion).build();

    AssumeRoleRequest assumeRequest = new AssumeRoleRequest()
        .withRoleArn(roleArnToAssume)
        .withRoleSessionName(DEFAULT_SESSION_NAME);
    logger.log("assume role request " + assumeRequest.toString());

    AssumeRoleResult assumeResult = stsClient.assumeRole(assumeRequest);
    logger.log("assume role response " + assumeResult.toString());

    //create a BasicSessionCredentials object the credentials you just retrieved.
    BasicSessionCredentials temporaryAWSCredentials = new BasicSessionCredentials(
        assumeResult.getCredentials()
            .getAccessKeyId(), assumeResult.getCredentials().getSecretAccessKey(),
        assumeResult.getCredentials()
            .getSessionToken());

    //provide temporary security credentials so that the amazon s3 client
    //can send authenticated requests to amazon s3. you create the client
    //using the sessionCredentials object.
    return AmazonS3ClientBuilder.standard()
        .withCredentials(new AWSStaticCredentialsProvider(temporaryAWSCredentials))
        .withRegion(clientRegion)
        .build();
  }

  /**
   * Move a file. does a copy and delete
   */
  public boolean move(AmazonS3 client, String bucketName, String srcKey, String targetKey) {

    try {

      client.copyObject(bucketName, srcKey, bucketName, targetKey);
      client.deleteObject(bucketName, srcKey);
    } catch (AmazonServiceException exception) {
      logger.log(
          "MOVE_EXCEPTION"
              + bucketName
              + "/"
              + targetKey
              + "["
              + exception.getErrorMessage()
              + "]"
      );
      return false;
    }
    return true;
  }

  private void moveToError(AmazonS3 client, String srcBucket, String srcKey) {
    String errorKey = srcKey.replace(TRANSMIT_PREFIX, ERROR_PREFIX);
    logger.log("Moving " + srcKey + " to " + errorKey);
    if (!move(client, srcBucket, srcKey, errorKey)) {
      logger.log("ERROR_MOVE_FAILED :" + srcBucket + "/" + errorKey);
    }
  }

}
