#!/usr/bin/env bash

################################################################################
##
##  Build/Deploy lambda
##
################################################################################

args=$(getopts -l "s3-bucket:,fuction-name:" -o "s:f:h" -- "$@")

eval set -- "$args"

while [ $# -ge 1 ]; do
  case "$1" in
          --)
            # No more options left
            shift
            break
            ;;
          -f|--function-name)
                  functionName="$2"
                  shift
                  ;;
          -s|--s3-bucket)
                  s3Bucket="$2"
                  shift
                  ;;
           -h)
                  echo "Display some help"
                  exit 0
                  ;;
   esac

   shift

done
#./gradlew clean build
#./gradlew packageBig -x test
echo ''
echo  'bucket name passed:'$s3Bucket
echo ''
#aws s3 cp ./build/distributions/lambda-s3-file-process.zip s3://$s3Bucket/lambda-s3-file-process.zip
#aws lambda update-function-code --function-name as222-cm-fileprocess-LF --region us-east-1 --s3-bucket s3-file-process-code --s3-key code/lambda-s3-file-process.zip
