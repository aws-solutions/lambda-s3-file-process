package com.lambda.s3.file.process;

import java.util.Date;

public class Test {


  public static void main(String[] args) {

    DateFunction dateFunction = () -> 8;
    System.out.println(dateFunction.process());
    System.out.println(dateFunction.sum(11, 22));
  }
}


@FunctionalInterface
interface DateFunction {

  int process();

  static Date now() {
    return new Date();
  }

  default String formatDate(Date date) {
    return date.toString();
  }

  default int sum(int a, int b) {
    return a + b;
  }
}
